// mocking react-navigation for all tests that uses it.
jest.mock('react-navigation', () => {
  return {
    createAppContainer: jest
      .fn()
      .mockReturnValue(function NavigationContainer(props) {
        return null;
      }),

    createStackNavigator: jest.fn(),

    NavigationActions: {
      navigate: jest.fn().mockImplementation(x => x),
    },
  };
});

global.fetch = jest.fn(
  () =>
    new Promise((resolve, reject) => {
      resolve({
        json: jest.fn(),
      });
    }),
);
