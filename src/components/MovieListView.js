// @flow
import React, {Component} from 'react';
import NavigationService from '../NavigationService';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Image,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
//https://image.tmdb.org/t/p/w500/hpgda6P9GutvdkDX5MUJ92QG9aj.jpg

class MovieListView extends Component {
  constructor(props) {
    super(props);
    this.onEndReachedCalledDuringMomentum = false;
  }

  renderItem = movie => {
    return (
      <TouchableOpacity
        onPress={() => NavigationService.navigate('MovieDetailsView', movie)}>
        <SafeAreaView style={styles.mainContainer}>
          <View>
            <Image
              style={styles.image}
              source={{
                uri: `https://image.tmdb.org/t/p/w500${movie.backdrop_path}`,
              }}
            />
          </View>
          <View>
            <Text>{movie.title}</Text>
            <Text>{movie.release_date}</Text>
          </View>
        </SafeAreaView>
      </TouchableOpacity>
    );
  };

  separator = () => {
    return <View style={styles.separator} />;
  };

  onEndReached = () => {
    const {loadMovies} = this.props;
    if (!this.onEndReachedCalledDuringMomentum) {
      //loadMovies();
      this.onEndReachedCalledDuringMomentum = true;
    }
  };
  onMomentumScrollBegin = () => {
    this.onEndReachedCalledDuringMomentum = false;
  };

  renderFooter = () => {
    if (!this.props.isFetching) {
      return null;
    }
    return <ActivityIndicator style={styles.indicator} />;
  };

  render() {
    const {movies} = this.props;

    return (
      <View style={styles.mainContainer}>
        <FlatList
          data={movies}
          renderItem={({item}) => this.renderItem(item)}
          keyExtractor={movie => movie.id.toString()}
          ItemSeparatorComponent={this.separator}
          initialNumToRender={8}
          maxToRenderPerBatch={2}
          onEndReachedThreshold={10}
          onMomentumScrollBegin={this.onMomentumScrollBegin}
          onEndReached={this.onEndReached}
          ListFooterComponent={this.renderFooter}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  separator: {
    height: 0.5,
    width: '100%',
    backgroundColor: '#C8C8C8',
  },
  indicator: {
    color: '#000',
  },
  image: {
    width: 50,
    height: 50,
  },
});

export default MovieListView;
