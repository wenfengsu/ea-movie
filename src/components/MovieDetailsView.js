// @flow
import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

const MovieDetailsView = props => {
  const {
    poster_path,
    title,
    release_date,
    overview,
  } = props.navigation.state.params;

  return (
    <View style={styles.mainConatiner}>
      <Image
        style={styles.poster}
        source={{
          uri: `https://image.tmdb.org/t/p/w500${poster_path}`,
        }}
      />
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.date}>{release_date}</Text>
      <Text style={styles.overview}>{overview}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  poster: {
    width: 500,
  },
  title: {
    height: 32,
  },
  date: {
    height: 32,
    color: 'grey',
  },
  overview: {
    flex: 1,
    flexDirection: 'column',
  },
});

export default MovieDetailsView;
