import React, {Component} from 'react';

import MovieListView from './MovieListView';
import {movieActions} from '../actions';
import {connect} from 'react-redux';

class MovieListViewContainer extends Component {
  constructor(props) {
    super(props);
    this.currentPage = 1;
    this.state = {
      movies: undefined,
      err: undefined,
    };
  }

  loadMovies() {
    if (this.props.movies.length > 0 && !this.props.isFetching) {
      this.currentPage++;
      this.props.getMovies(this.currentPage);
    }
  }

  componentDidMount() {
    this.props.getMovies(1);
  }

  render() {
    return (
      <MovieListView
        movies={this.props.movies}
        loadMovies={() => this.loadMovies()}
        isFetching={this.props.isFetching}
      />
    );
  }
}

export default connect(
  // all these could be implemented using selectors
  state => ({
    movies: state.movie.movies ? state.movie.movies.results : [],
    page: state.movie.movies ? state.movie.movies.page : 1,
    isFetching: state.movie.movies ? state.movie.movies.fetchPending : false,
  }),
  {...movieActions},
)(MovieListViewContainer);
