const URL =
  'https://api.themoviedb.org/3/discover/movie?api_key=49e54208fd98be0fc94a62ed265efcd5';

const ENDPOINTS = {
  popularMovies: p => `${URL}&sort_by=popularity.desc&page=${p}`,
};

export const getMovies = page => {
  if (!page) {
    page = 1;
  }

  return new Promise((resolve, reject) => {
    fetch(ENDPOINTS.popularMovies(page))
      .then(response => response.json())
      .then(responseJson => {
        resolve(responseJson);
      })
      .catch(error => {
        reject(error);
      });
  });
};
