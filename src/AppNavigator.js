// @flow
import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import NavigationService from './NavigationService';
import {MovieDetailsView, MovieListViewContainer} from './components';
import {createStackNavigator, createAppContainer} from 'react-navigation';

class AppNavigator extends Component {
  render() {
    const AppNav = createStackNavigator(
      {
        MovieListViewContainer: {
          screen: MovieListViewContainer,
        },
        MovieDetailsView: {
          screen: MovieDetailsView,
        },
      },
      {
        mode: 'card',
        initialRouteName: 'MovieListViewContainer',
      },
    );

    const AppContainer = createAppContainer(AppNav);
    return (
      <View style={styles.container}>
        <AppContainer
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default AppNavigator;
