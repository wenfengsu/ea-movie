import {createStore, combineReducers, compose, applyMiddleware} from 'redux';
import thunkMiddlware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {movie} from './reducers';

const rootReducer = combineReducers({
  movie,
});

const loggerMiddleware = createLogger();

const store = createStore(
  rootReducer,
  undefined,
  compose(applyMiddleware(thunkMiddlware, loggerMiddleware)),
);

export default store;
