import * as movie from '../movie';
import result from './fixtures/movieResults';

import * as api from '../../api/movie';
jest.mock('../../api/movie');

describe('Movie Redux Actions', () => {
  it('get top movies with page', async () => {
    const dispatch = jest.fn();
    api.getMovies = jest
      .fn()
      .mockImplementation(() => new Promise.resolve(result));

    const thunkfn = movie.getMovies(1);
    await thunkfn(dispatch).then(resp => {
      expect(dispatch.mock.calls[0][0]).toMatchObject({
        type: 'FETCH_TOP_MOVIES.PENDING',
      });
      expect(dispatch.mock.calls[1][0]).toMatchObject({
        type: 'FETCH_TOP_MOVIES.FULFILLED',
        payload: result,
      });
    });

    expect(api.getMovies.mock.calls.length).toBe(1);
  });
});
