import * as api from '../api';

export const getMovies = page => dispatch => {
  dispatch({type: 'FETCH_TOP_MOVIES.PENDING'});
  return api
    .getMovies(page)
    .then(payload => dispatch({type: 'FETCH_TOP_MOVIES.FULFILLED', payload}))
    .catch(e => {
      console.warn('error: ', e);
      if (e.code >= 500) {
        dispatch({type: 'INTERNAL_SERVER_ERROR', payload: e});
      } else {
        dispatch({type: 'FETCH_TOP_MOVIES.REJECTED', payload: e});
      }
    });
};
