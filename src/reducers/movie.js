const DEFAULT_STATE = {movies: null, fetchPending: false};

export default (state = DEFAULT_STATE, {type, payload}) => {
  switch (type) {
    case 'FETCH_TOP_MOVIES.PENDING':
      return {...state, fetchPending: true};
    case 'FETCH_TOP_MOVIES.FULFILLED':
      const downloadedMovies =
        state && state.movies ? state.movies.results : [];
      return {
        ...state,
        fetchPending: false,
        movies: {
          ...payload,
          results: [...downloadedMovies, ...payload.results],
        },
      };
    case 'FETCH_TOP_MOVIES.REJECTED':
      return {...state, fetchError: payload, fetchPending: false};
    default:
      return state;
  }
};
