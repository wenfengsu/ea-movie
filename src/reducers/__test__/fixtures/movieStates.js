export default {
  movie: {
    pending: {movies: null, fetchPending: false},
    fulfilled: {
      movies: {results: ['someMovieData1', 'someMovieData2']},
      fetchPending: false,
    },
    rejected: {error: new Error('rejectedReason')},
  },
};
