export default {
  movie: {
    pending: {
      type: 'FETCH_TOP_MOVIES.PENDING',
    },
    fulfilled: {
      type: 'FETCH_TOP_MOVIES.FULFILLED',
      payload: {
        results: ['someMovieData3', 'someMovieData4'],
      },
    },
    rejected: {
      type: 'FETCH_TOP_MOVIES.REJECTED',
      payload: new Error('Rejected reason'),
    },
  },
};
