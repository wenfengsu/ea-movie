export default {
  movie: {
    pending: {movies: null, fetchPending: true},
    fulfilled: {
      fetchPending: false,
      movies: {
        results: [
          'someMovieData1',
          'someMovieData2',
          'someMovieData3',
          'someMovieData4',
        ],
      },
    },
    rejected: {fetchError: new Error('Rejected reason'), fetchPending: false},
  },
};

// {...state, fetchError: payload, fetchPending: false};
