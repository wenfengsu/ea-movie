import movie from '../movie';
import actions from './fixtures/movieActions';
import results from './fixtures/movieResults';
import states from './fixtures/movieStates';

describe('Movie Reducer', () => {
  it('Movie Pending', () => {
    const result = movie(states.movie.pending, actions.movie.pending);
    expect(result).toMatchObject(results.movie.pending);
  });

  it('Movie Fulfilled', () => {
    const result = movie(states.movie.fulfilled, actions.movie.fulfilled);
    expect(result).toMatchObject(results.movie.fulfilled);
  });

  it('Movie Rejected', () => {
    const result = movie(states.movie.rejected, actions.movie.rejected);
    expect(result).toMatchObject(results.movie.rejected);
  });
});
